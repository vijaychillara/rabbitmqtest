package com.vchillara.RabbitMQTest.service;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import  com.vchillara.RabbitMQTest.model.Employee;


@Component
public class RabbitMQConsumer {

	@RabbitListener(queues = "vchillara.queue")
	public void recievedMessage(Employee employee) {
		System.out.println("Recieved Message From RabbitMQ: " + employee);
	}

}
