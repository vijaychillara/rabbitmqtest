package com.vchillara.RabbitMQTest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.amqp.core.AmqpTemplate;
import com.vchillara.RabbitMQTest.model.Employee;

@Service
public class RabbitMQSender {

	@Autowired
	private AmqpTemplate rabbitTemplate;

	final private String exchange="vchillara.exchange";

	
	final private String routingkey="vchillara.routingkey";	
	
	public void send(Employee company) {
		rabbitTemplate.convertAndSend(exchange, routingkey, company);
		System.out.println("Exchange" + exchange + ", Key" + routingkey);

		System.out.println("Send msg = " + company);
	    
	}

}
